﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base_Derived_Interfaces_Inheritance_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //so I have a interface
            //I also have a class that implements that interface
            //Now, lets use it.

            IWater_Bottle temp_bottle_1 = new Water_Bottle_1();

            Water_Bottle_1 temp_bottle_2 = new Water_Bottle_1();

            //since the methods are returning an object, I am casting them to strin
            //because I know they are returning strings from the implementation below
            string o1 = (string)temp_bottle_1.return_quantity();

            string o2 = (string)temp_bottle_2.return_quantity();

            Console.WriteLine(" Main - stuff in o1 is {0} and o2 is {1}", o1, o2);

            //temp_bottle_2 is of type Water_Bottle_1. so it can use
            //say_something()
            string o3 = (string)temp_bottle_2.say_something();
            //the following will give a error
            //temp_bottle_1.say_something()

            //thats because say_something is a member of Water_Bottle_1 although 
            //Water_Bottle is implement the interface IWAter_Bottle via Water_Bottle1

            //now creating a type of Interface IWAterBottle with the Water_Bottle_2 implementation
            IWater_Bottle temp_bottle_3 = new Water_Bottle_2();
            //earlier, I was collecting the quantity in a string type
            //however, now I am collecting it as double
            //although, both temp_bottle_1 and temp_bottle3 are coming from the same interface
            double o4 = (double)temp_bottle_3.return_quantity();

            //this is to stop the console window from dissapearing
            Console.ReadLine();

        }
    }

    //defining a simple interface
    //it has a property and a method signature (not a definition) that deals with 
    //that property
    //this interface tells anyone that uses it that there is a method that will return quantity
    //and 
    public interface IWater_Bottle
    {
        //a property
        string quantity { get; set; }

        //a method that converts quantity
        //I am using object type becuase this method can now work with any return type
        //like string, int, double and so on.
        object return_quantity();

    }

    //I am implementing the interface via inheritance (of course dont get confused between 
    //inheritance and interfaces)
    //if you dont implement all the members of the interface, the compiler will warn you
    //I usually hover over the error and let visual studio fill all the code for me
    //you should probably do the same
    class Water_Bottle_1 : IWater_Bottle
    {
        private string _quantity;

        //a constructor to set the value
        public Water_Bottle_1()
        {
            quantity = "10";
        }


        //this is part of the interface that I am implementing/deriving
        public string quantity
        {
            get
            {
                return _quantity;
            }

            set
            {
                //first check for null or empty
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentException();
                _quantity = value;
            }
        }

        //this is part of the interface that I am implementing/deriving
        public object return_quantity()
        {
            string temp = "The amount of water in the bottle is " + quantity;
            return temp;
        }

        //this is not part of the interface that I am implementing/deriving
        //that means, only objects of the type to which this belongs can access this
        //interface objects cannot touch this
        public object say_something()
        {
            return "I am saying something";
        }
    }

    //now I will define another class that will implement the same interface differently
    //this is where the code reuse magic happens
    class Water_Bottle_2 : IWater_Bottle
    {
        //in the previous class I used a string _quantity.
        //now I am using a double
        double _quantity;

        public Water_Bottle_2()
        {
            quantity = "20";
        }

        public string quantity
        {
            get
            {
                return _quantity.ToString();
            }

            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentException();
                //I will convert the string into it's equivalent double value
                _quantity = Convert.ToDouble(value);
            }
        }

        //the previous implementation returned a sentence
        //in this implementation I am simply returning a double value
        public object return_quantity()
        {
            return (_quantity);
        }
    }
}
